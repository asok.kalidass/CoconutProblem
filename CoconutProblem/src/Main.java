import java.util.Scanner;

/**
 * Represents a solution to the given problem that calculates minimum number of required coconuts for sailors
 * <p>
 * Coconut problem have following rules
 * <ul>
 * <li> At any point of time upon dividing the coconuts into equal parts there should be always one left over for monkey
 * </ul>
 * <p>
 * Assumptions/Restrictions: This program has the ability to calculate the minimum number of coconuts for range of 2 to 8 sailors and
 * The User should inputs the valid sailor between 2 to 8
 * <p>
 * Noteworthy Features: Self incrementing loop to check the minimum coconuts need for the sailor crowd
 * 
 * @author - Asok Kalidass Kalisamy - Graduate Student ( B00763356 )
 */
public class Main {
    //Declarations
	static int sailorsSleeping = 0;
	static int coconutsRemaining = 0;
	static int sailorsPart = 0;
	static int coconuts = 1;
	static int remainder = 0;
	public static void main(String[] args) 
	{
		@SuppressWarnings("resource")
		Scanner getSailor = new Scanner(System.in);
		System.out.print("Enter the number of sailors");
		int sailors = getSailor.nextInt();
		//calculates the bare minimum coconut for the sailor 
		calculateBalanceCoconut(sailors);
	}
	/*
	 *It identifies the minimum coconuts needed to satisfy the problem definition 
	 *@param - Sailors - sailors crew for which minimum number of coconut has to be determined.
	 */
	static void calculateBalanceCoconut(int sailors)
	{
		while(coconuts > 0)
		{
			//check the minimum coconuts need for the sailor crowd
			for (int i = 0; i < sailors; i++)
			{								
				if (i == 0)
				{
					sailorsPart = coconuts / sailors;
					boolean IsValidCoconut = testCoconuts(sailors, (sailors - (i +1)), coconuts);
					
					if ((!IsValidCoconut) && (remainder != 1))
					{
						break;
					}
				}
				else
				{
					sailorsPart = coconutsRemaining / sailors;
				}
                //remaining coconuts left after sailor takes his part.
				coconutsRemaining = sailorsPart * (sailors - 1);				
                //validate the coconut 
				boolean IsValidCoconut = testCoconuts(sailors, (sailors - (i +1)), coconutsRemaining);
				if(IsValidCoconut)
				{
					System.out.print("The minimum number of coconuts required :");	
					System.out.print(coconuts);	
					return;
				}
				else if (!((coconutsRemaining % sailors) == 1))
				{
					break;
				}								
			}
		}
	}
    /*
     *It validates whether there are remaining one coconuts left over for monkey 
     *@param sailors - sailors crew for which minimum number of coconut has to be determined
     *@param sailorsSleeping - The left over sailors 
     *@param coconutsRemaining - Remaining coconuts for left over sailors
     *@return IsValidCoconuts - true if coconuts are valid enough to split as per problem statement else false
     */
	public static boolean testCoconuts(int sailors, int sailorsSleeping, int coconutsRemaining)
	{
		boolean IsValidCoconuts = false;
		remainder = coconutsRemaining % sailors;
		switch(remainder)
		{
		case 1: //means there is one coconut left after splitting into 2 parts
			if (sailorsSleeping == 0)
			{
				IsValidCoconuts = true;
			}
			else
			{
				IsValidCoconuts = false;
			}
			break;
		case 0: // means the coconuts is exactly divided and there is no left over. So increment the coconut count
		{
			coconuts++;
			IsValidCoconuts = false;
			break;
		}
		default: // //means there is more coconut left after splitting into 2 parts. So increment the coconut count
		{
			coconuts++;
			IsValidCoconuts = false;
			break;
		}

		}	
		return IsValidCoconuts;
	}
}

